from dal import autocomplete
from django.contrib import messages
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render
# Create your views here.
from django.urls import reverse
from django.views.generic import ListView, FormView

from discount.forms import SearchPropertyForm
from discount.models import Discount, Customer, Product


def index(request):
    return render(request, "index.html")


class DiscountListView(ListView):
    queryset = Discount.objects.all()
    context_object_name = 'discount_list'
    paginate_by = 2
    template_name = 'discount/discount_list.html'

    def get_context_data(self, **kwargs):
        user_id = self.kwargs['user_id']
        context = super(DiscountListView, self).get_context_data(**kwargs)
        context['user'] = Customer.objects.get(id=user_id)
        context['discounts'] = Discount.objects.filter(user=context['user'])
        context['products'] = Product.objects.filter(id__in=context['discounts'].values('product'))
        return context


class CustomerAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Customer.objects.none()

        qs = Customer.objects.all()

        if self.q:
            qs = qs.filter(Q(user__email__istartswith=self.q) | Q(user__username__istartswith=self.q) | Q(
                user__first_name__istartswith=self.q) | Q(user__last_name__istartswith=self.q))

        return qs


class ProductAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Product.objects.none()

        qs = Product.objects.all()

        if self.q:
            qs = qs.filter(Q(name__istartswith=self.q) | Q(price__istartswith=self.q))

        return qs


class SetDiscountView(FormView):
    form_class = SearchPropertyForm
    template_name = "discount/set_discount.html"

    def get_success_url(self):
        return reverse('discount:set_discount')

    def form_valid(self, form):
        user = form.cleaned_data['user']
        product = form.cleaned_data['product']
        discount = Discount.objects.create_discount(user=user, product=product)
        discount.save()
        messages.success(self.request, 'Discount successfully added ' + discount.code)
        return super(SetDiscountView, self).form_valid(form)


def get_customer_info(request):
    user_id = None
    if request.method == 'GET':
        user_id = request.GET['user_id']
        customer = Customer.objects.get(id=int(user_id))
        print(customer.photo.url)
    return JsonResponse(
        {'first_name': customer.user.first_name, 'last_name': customer.user.last_name, 'email': customer.user.email,
         'location': customer.location, 'phone': customer.phone,
         'photo': customer.photo.url if customer.photo else None})
