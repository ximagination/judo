from django.conf.urls import url

from discount.views import DiscountListView, CustomerAutocomplete, ProductAutocomplete, SetDiscountView, \
    get_customer_info

urlpatterns = [
    url(r'^set_discount/$', SetDiscountView.as_view(), name='set_discount'),
    url(r'^discount_list/(?P<user_id>\d+)/', DiscountListView.as_view(), name='discount_view'),
    url(r'^customer-autocomplete/$', CustomerAutocomplete.as_view(), name='customer-autocomplete', ),
    url(r'^product-autocomplete/$', ProductAutocomplete.as_view(), name='product-autocomplete', ),
    url(r'^customer_info/$', get_customer_info, name='get_customer_info'),
]
