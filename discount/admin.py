from django.contrib import admin
# Register your models here.
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from discount.models import Category, Product, Discount, Customer, Staff


class CustomerInline(admin.StackedInline):
    model = Customer
    can_delete = False
    verbose_name_plural = 'Customer'
    fk_name = 'user'

class StaffInline(admin.StackedInline):
    model = Staff
    can_delete = False
    verbose_name_plural = 'Staff'
    fk_name = 'user'

class CustomUserAdmin(UserAdmin):
    inlines = (CustomerInline, StaffInline )
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'get_location', 'get_phone', 'get_photo')
    list_select_related = ('customer', 'staff')

    def get_location(self, instance):
        return instance.customer.location
    get_location.short_description = 'Location'

    def get_phone(self, instance):
        return instance.customer.phone if instance.customer else instance.staff.phone
    get_phone.short_description = 'Phone'

    def get_photo(self, instance):
        return instance.customer.photo
    get_photo.short_description = 'Photo'

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('user', 'location', 'birthdate', 'photo', 'bio', 'phone')
    list_filter = ('location', 'birthdate')
    search_fields = ('user', 'location')
    fields = ('user', 'location', 'birthdate', 'photo', 'bio', 'phone')

class StaffAdmin(admin.ModelAdmin):
    list_display = ('user', 'phone')
    search_fields = ('user', )
    fields = ('user', 'phone')

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', )
    list_filter = ('name',)
    fields = ('name', 'description',)
    search_fields = ('name',)


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'price', 'image' )
    list_filter = ('name', 'category', 'price',)
    fields = ('name', 'description', 'category', 'price', 'image' )
    search_fields = ('name', 'category',)

class DiscountAdmin(admin.ModelAdmin):
    list_display = ('user', 'product', 'code', )
    list_filter = ('user', 'product',)
    fields = ('user', 'product', 'code', )
    search_fields = ('user', 'product',)
    can_delete = False
    list_select_related = ('product', 'user',)

    def has_add_permission(self, request):
        return False

    def get_readonly_fields(self, request, obj=None):
        result = list(set(
            [field.name for field in self.opts.local_fields] +
            [field.name for field in self.opts.local_many_to_many]
        ))
        result.remove('id')
        return result


admin.site.register(Customer, CustomerAdmin)
admin.site.register(Staff)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Discount, DiscountAdmin)