import random
import string

from django.contrib.auth.models import User
from django.db import models
from django.db.models import ImageField
from django.utils.translation import ugettext as _


# Create your models here.


class Customer(models.Model):
    user = models.OneToOneField(User, null=True, related_name='customer')
    location = models.CharField(max_length=30, blank=True)
    birthdate = models.DateField(null=True, blank=True)
    photo = ImageField(verbose_name=_("Profile Picture"),
                       max_length=255, null=True, blank=True, upload_to='user_pictures')
    bio = models.TextField(default='', blank=True)
    phone = models.CharField(max_length=20, blank=True, default='')

    class Meta:
        verbose_name = _("Customer")
        verbose_name_plural = _("Customers")

    def __str__(self):
        return '{}--{}--{} {}'.format(self.user.username, self.user.email, self.user.first_name, self.user.last_name)


class Staff(models.Model):
    user = models.OneToOneField(User, null=True, related_name='staff')
    phone = models.CharField(max_length=20, blank=True, default='')

    class Meta:
        verbose_name = _("Staff Member")
        verbose_name_plural = _("Staff Members")

    def __str__(self):
        return self.user.username


class Category(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=500, blank=True, null=True)
    category = models.ForeignKey(Category, null=True, blank=True)
    price = models.CharField(max_length=30)
    image = models.ImageField(null=True, blank=True, upload_to='products')

    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    def __str__(self):
        return '{}--{}'.format(self.name, self.price)


class DiscountManager(models.Manager):
    def create_discount(self, user, product, code=''):
        book = self.create(user=user, product=product, code=self.unique_code())
        # do something with the book
        return book

    def code_generator(self):
        result = []
        for x in range(4):
            choice = random.choice([True, False])
            if choice:
                result.append(random.choice(string.ascii_letters + string.digits))
            else:
                result.append(random.randrange(0, 9, 1))
        return '{0}-{1}-{2}-{3}'.format(*result)

    def unique_code(self):
        code = self.code_generator()
        try:
            Discount.objects.get(code=code)
        except Discount.DoesNotExist:
            return code
        else:
            self.unique_code()

class Discount(models.Model):
    code = models.CharField(max_length=7, unique=True)
    product = models.ForeignKey(Product)
    user = models.ForeignKey(Customer)

    objects = DiscountManager()
    class Meta:
        verbose_name = _("Discount")
        verbose_name_plural = _("Discounts")
