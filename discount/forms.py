from dal import autocomplete
from django import forms

from discount.models import Product, Customer, Discount


class SearchPropertyForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=Customer.objects.all(),
                                  help_text='Please, enter user name or email',
                                  widget=autocomplete.ModelSelect2(url='discount:customer-autocomplete'))
    product = forms.ModelChoiceField(queryset=Product.objects.all(),
                                     help_text='Please, enter product name or price',
                                     widget=autocomplete.ModelSelect2(url='discount:product-autocomplete'))

    class Meta:
        model = Discount
        exclude = ('code',)
