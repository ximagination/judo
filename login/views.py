from django.contrib import messages
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic.base import View
from django.views.generic.edit import FormView

from discount.models import Customer, Staff


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "login/login_form.html"
    success_url = "/"

    def get_success_url(self):
        super().get_success_url()
        try:
            id = self.request.user.customer.id
            return reverse('discount:discount_view', args={id})
        except Customer.DoesNotExist:
            try:
                id = self.request.user.staff
                return reverse('discount:set_discount')
            except Staff.DoesNotExist:
                messages.error(self.request, 'You are not registered',
                               extra_tags='success')
                return ('/')

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        messages.error(self.request, 'Congratulations you have logined  successfully to our site', extra_tags='success')
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        messages.error(request, 'You are logout', extra_tags='info')
        return HttpResponseRedirect("/")
